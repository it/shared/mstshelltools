#!/usr/bin/perl

# Begin-Doc
# Description: wrapper script for auto-login to mysqldump using authsrv stashes
# End-Doc

use strict;
use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use Local::AuthSrv;
use Getopt::Long qw(:config no_auto_abbrev pass_through);

my $help;
my $debug;
my $owner;
my $user;
my $instance;
my $res = GetOptions(
    "help+"      => \$help,
    "debug+"     => \$debug,
    "owner=s"    => \$owner,
    "user=s"     => \$user,
    "instance=s" => \$instance
);

if ( !$res || $help ) {
    print "Usage: $0 [--help] [--debug] [--owner userid] [--user userid] [--instance instance]\n";
    exit(0);
}

my $rest = join( " ", @ARGV );

if ( !$user ) {
    if ( $rest =~ /-u\s*(.+?)\b/ ) {
        $user = $1;
    }
}
if ( !$user ) {
    $user = ( getpwuid($<) )[0];
}
if ( !$owner ) {
    $owner = $user;
}

my $pw;

if ($instance) {
    $pw = &AuthSrv_Fetch( "owner" => $owner, "user" => $user, "instance" => $instance );
}
if ( !$pw && $instance ne "mysql" ) {
    $pw = &AuthSrv_Fetch( "owner" => $owner, "user" => $user, "instance" => "mysql" );
}
if ( !$pw ) {
    print "Failed to load password.\n";
    exit(1);
}

my $fifo = "/dev/shm/mstmysqldump-fifo-$$-" . time . "-" . rand;
system( "mknod", "-m", "0600", $fifo, "p" ) && die "Failed to create fifo.";

if ( !fork() ) {
    open( my $fh, ">$fifo" );
    print $fh "[client]\n";
    print $fh "password=$pw\n";
    close($fh);
    exit(0);
}

system( "mysqldump", "--defaults-extra-file=$fifo", @ARGV );
unlink($fifo);
exit;