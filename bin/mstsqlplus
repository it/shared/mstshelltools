#!/usr/bin/perl

# Begin-Doc
# Description: wrapper script for auto-login to sqlplus using authsrv stashes
# End-Doc

use strict;
use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use Local::AuthSrv;
use Getopt::Long;
use Expect;

my $help;
my $debug;
my $owner;
my $user;
my $instance;
my $res = GetOptions(
    "help+"      => \$help,
    "debug+"     => \$debug,
    "owner=s"    => \$owner,
    "user=s"     => \$user,
    "instance=s" => \$instance
);

if ( !$res || $help ) {
    print "Usage: $0 [--help] [--debug] [--owner userid] [--user userid] [--instance instance]\n";
    exit(0);
}

my $rest = join( " ", @ARGV );

if ( !$user ) {
    if ( $rest =~ /\b(.*?)\@/ ) {
        $user = $1;
    }
}
if ( !$user ) {
    $user = ( getpwuid($<) )[0];
}
if ( !$owner ) {
    $owner = $user;
}

print "Will look for $user under $owner with instance $instance\n";

my $pw;

if ($instance) {
    $pw = &AuthSrv_Fetch( "owner" => $owner, "user" => $user, "instance" => $instance );
}
if ( !$pw && $instance ne "oracle" ) {
    $pw = &AuthSrv_Fetch( "owner" => $owner, "user" => $user, "instance" => "oracle" );
}
if ( !$pw && $instance ne "ads" ) {
    $pw = &AuthSrv_Fetch( "owner" => $owner, "user" => $user, "instance" => "ads" );
}
if ( !$pw ) {
    print "Failed to load password.\n";
    exit(1);
}

my $exp;
$exp = Expect->spawn( "sqlplus", @ARGV ) || die "cannot open sqlplus session";
$exp->debug(0);

if ( $exp->expect( 10, "Enter password:" ) ) {
    print "\n";
    print "Saw prompt, sending password.\n";
    $exp->send( $pw, "\n" );

    if ( $exp->expect( 2, "SQL>" ) ) {
        print "\nSaw prompt, switching to interactive mode.\n";
        $exp->send("\n");
        $exp->interact();
        print "Done with interactive mode.\n";
        exit(0);
    }
    else {
        print "Failed to log in, exiting.\n";
        exit(1);
    }
}
else {
    print "Did not see prompt.\n";
}